<?php
// Initialize the session
session_start();
require_once "./../login/config.php";
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
?>

<html>

<head>
    <script src="alert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="alert/dist/sweetalert.css">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style>
    /* width */
    ::-webkit-scrollbar {
        width: 3px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        background: #f1f1f1;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #888;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        background: #555;
    }

    input {
        width: 80% !important;
    }

    .calendar {
        position: relative;
        width: 720px;
        display: flex;
    }

    div.date,
    div.days {
        width: 90px;
        border: 1px solid black;
        float: left;
        margin: 1px;
    }

    .blankday {
        background: linear-gradient(90deg, #ffb3ba, #ffdfba, #ffffba, #baffc9, #bae1ff);
    }

    div.date {
        height: 90px;
        overflow: auto;
    }

    .today {
        background: #cfc;
    }

    #form {
        position: fixed;
        top: 30%;
        left: 83%;
    }

    #showapp {
        margin-left: 17%;
    }

    .time-list {
        padding: 2px;
        text-align: center;
        font-size: 13px;
        width: 6vw;
        margin-top: -2%;
    }

    .app {
        margin-left: 30px;
    }
    </style>
</head>
<?php
if(isset($_GET['date'])){
    $day = date('d', strtotime($_GET['date'])); //Gets day of appointment (1‐31)
    $month = date('m', strtotime($_GET['date'])); //Gets month of appointment (1‐12)
    $year = date('Y', strtotime($_GET['date'])); //Gets year of appointment (e.g. 2016)
    $days = date('t', strtotime($_GET['date'])); //Gets number of days in month
    $firstday = date('w', strtotime('01-'.$month.'-'.$year)); //Gets the day of the week for the 1st of
    //the month. (e.g. 0 for Sun, 1 for Mon)
    $nmonth = strtotime($_GET['date']);
}else{
    $day = date('d', strtotime(date("Y-m-d"))); //Gets day of appointment (1‐31)
    $month = date('m', strtotime(date("Y-m-d"))); //Gets month of appointment (1‐12)
    $year = date('Y', strtotime(date("Y-m-d"))); //Gets year of appointment (e.g. 2016)
    $days = date('t', strtotime(date("Y-m-d"))); //Gets number of days in month
    $firstday = date('w', strtotime('01-'.$month.'-'.$year)); //Gets the day of the week for the 1st of
    //the month. (e.g. 0 for Sun, 1 for Mon)
    $nmonth = strtotime(date("Y-m-d"));
}
    $longdate = $year."-".$month."-".$day;
    $today = date('d'); //Gets today’s date
    $todaymonth = date('m'); //Gets today’s month
    $todayyear = date('Y'); //Gets today’s year
    $userID = $_SESSION['id'];
    $sql = "SELECT Appointment,StartTime,EndTime,AppointmentID FROM appointment WHERE UserID = '$userID' AND Date='$longdate'"; //sql command get data
    $monthName = date("F", mktime(null, null, null, $month)); //change number to name month
	$nextday = date('Y-m-d',strtotime('+1 day', $nmonth));
    $prevday = date('Y-m-d',strtotime('-1 day', $nmonth));
    $app_list = array();
    $st_list = array();
    $et_list = array();
    $appid_list = array();
    $app = mysqli_query($link, $sql); //make sql command active
    while($row = mysqli_fetch_assoc($app)) { //access to each appointment
        array_push($app_list, $row['Appointment']);
        array_push($st_list, $row['StartTime']);
        array_push($et_list, $row['EndTime']);
        array_push($appid_list, $row['AppointmentID']);
    }
    $dayname = date('D', strtotime($longdate));
?>

<body>
    <div class="page-header">

        <a href="?date=<?php echo $prevday;?>"><i class="material-icons">navigate_before</i></a>
        <?php
            echo("$dayname"." ");
            echo("$day"." ");
            echo("$monthName"." ");
            echo("$year"." ");
      
        ?>
        <a href="?date=<?php echo $nextday;?>"><i class="material-icons">navigate_next</i></a>
        <a href="./../login/logout.php" class="black waves-effect waves-light btn">
            logout
        </a>
        <a href="dayview.php" class="blue waves-effect waves-light btn"> Today</a>
        <a href="monthview.php" class="blue waves-effect waves-light btn"> Month View </a>
        <a href="weekview.php" class="blue waves-effect waves-light btn"> Week View </a>
        <a href="#" class="blue waves-effect waves-light btn"> Day View </a>
    </div>



    <div class="calendar">
        <div id="list-example" class="list-group" style="position: fixed;">
            <?php
            for($i = 0; $i < 24 ; $i++){
                $time;
                if($i<10){
                    $time = "0".$i.":00";
                }else{
                    $time = $i.":00";
                }

                echo '<a class="time-list list-group-item list-group-item-action" href="#list-item-'.$i.'">'.$time.'</a>'; //slot time
            }
        
        ?>
        </div>
        <div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example" id="showapp">
            <?php
            
                for($i = 0; $i < 24 ; $i++){
                    echo '<br>';
                    $time;
                    if($i<10){
                        $time = "0".$i.":00";
                    }else{
                        $time = $i.":00";
                    }

                    echo '<h5 id="list-item-'.$i.'">'.$time.'</h5>'; //displayed time
                    for($j = 0; $j < sizeof($st_list); $j++){
                        if( $i ==  date("H", strtotime($st_list[$j])) ){
                            //show appointment
                            echo ' 
                            <div class="row row-cols-2">
                                <div class="col-7">
                                    <b>'.$app_list[$j].'</b> '.$st_list[$j].'-'.$et_list[$j].'
                                </div>
                                <div class="col-5"> 
                                    <form action = "./delete_app_dayview.php" method="post" class="app">                           
                                        <input name="appy" type="number" value="'.$appid_list[$j].'" hidden>
                                        <input name="Date" type="date" value="'.$longdate.'" hidden> 
                                        <input value="Delete" type="submit">
                                    </form>
                                </div>
                          </div>';
                        }
                    }
                    echo "<br>";
                }
            
            ?>
        </div>
    </div>

    <div id="form">
        <form action="add_app_dayview.php" method="post">
            <input name="Date" type="date"><br>
            From: <input name="StartTime" type="time"><br>
            To: <input name="EndTime" type="time"><br>
            <input name="Assi" type="text" placeholder="Enter Title"><br>
            <input type="submit" value="Submit">
        </form>
    </div>
</body>



<?php
mysqli_close($link);
?>

<script>
function monthAppointment(Appointment, StartTime, EndTime, AppointmentID, Date) {
    document.getElementById("box").innerHTML += Appointment + "<br/>"
    document.getElementById("box").innerHTML += StartTime + "<br/>"
    document.getElementById("box").innerHTML += EndTime + "<br/>"
    document.getElementById("box").innerHTML += Date + "<br/>"
    document.getElementById("appy").value = AppointmentID;
    document.getElementById("appyDate").value = Date;
}
</script>